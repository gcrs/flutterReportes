import 'package:flutter/material.dart';
import 'package:flutter_watch_shop/global_widgets/custom_appbar.dart';
import 'package:flutter_watch_shop/models/product.dart';
import 'package:flutter_watch_shop/utils/utils.dart';
import 'package:flutter_watch_shop/views/home/widgets/product_list.dart';
import 'package:flutter_watch_shop/views/home/widgets/product_list.dart' as prefix0;
import 'package:geolocator/geolocator.dart';
import 'package:toast/toast.dart';
import 'package:camera/camera.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:camera/camera.dart';



class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  List<String> tabs = ["Reportes"];//, "Sports", "Smart"];

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: tabs.length);
  }

  @override
  Widget build(BuildContext context) {
    final tabBar = TabBar(
      controller: tabController,
      indicatorColor: Theme.of(context).primaryColor,
      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(width: 3.0),
        insets: EdgeInsets.symmetric(horizontal: 55.0),
      ),
      labelColor: Theme.of(context).primaryColor,
      labelStyle: TextStyle(fontSize: 22.0, fontFamily: AppFonts.primaryFont),
      unselectedLabelColor: Theme.of(context).primaryColor.withOpacity(0.3),
      tabs: tabs.map((tabName) => Tab(text: tabName)).toList(),
      
    );

    final tabBarView = Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        margin: EdgeInsets.only(top: 30.0),
        child: TabBarView(
          controller: tabController,
          children: <Widget>[ProductList()],//, ProductList(), ProductList()],
          
        ),
      ),
    );
    String subs(String path){
      path.replaceAll("'","");
      return path;
    }
void getLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    List<Placemark> placemark=await Geolocator().placemarkFromCoordinates(position.latitude, position.longitude);
    Toast.show("Sus coordenadas."+position.toString(), context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      var picture = await ImagePicker.pickImage(source: ImageSource.camera,);
      print(picture);
      print(picture.toString().substring(7,145));
    ProductList().setnewProduct(
      new Product(
        id: ProductList().getListPr().length+1,
         name: "R#100"+(ProductList().getListPr().length+1).toString(),
         lat: position.latitude,
         lon: position.longitude,
         price: position.latitude.toString().substring(0,7)+", "+position.longitude.toString().substring(0,7),
         photos: [subs(picture.toString().substring(7,picture.toString().length-1)),subs(picture.toString().substring(7,picture.toString().length-1))],
         colors: ["#FE0800", "#008C3B"],
         brand: placemark[0].subLocality.toString(),
      )
    );
  }
Future fabPressed() async {
  getLocation();
  build(this.context);
        new Opacity(opacity: 0.0, child: tabBarView
            );



}

    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 50.0),
        child: Column(
          children: <Widget>[tabBar, tabBarView],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
      backgroundColor: const Color(0xFF0099ed),
      child: new Icon(Icons.loupe),
      onPressed: fabPressed)
    );
  }
}
