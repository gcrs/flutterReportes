import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_watch_shop/global_widgets/custom_appbar.dart';
import 'package:flutter_watch_shop/models/product.dart';
import 'package:flutter_watch_shop/services/alert.service.dart';
import 'package:flutter_watch_shop/utils/colors.dart';
import 'package:flutter_watch_shop/views/product_details/widgets/color_chooser.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:toast/toast.dart';

class ProductDetailsPage extends StatefulWidget {
  final Product product;

  const ProductDetailsPage({Key key, @required this.product}) : super(key: key);

  @override
  _ProductDetailsPageState createState() => _ProductDetailsPageState();
}

class _ProductDetailsPageState extends State<ProductDetailsPage> {
    void selection(){
      setState(() {
        if(widget.product.id!=3){
            print(widget.product.id);
          _selectedColorIndex = 0;
          }else{
             _selectedColorIndex = 1;
          }
      });
    }
  int _selectedColorIndex = 0;
  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
       //Se utiliza para hacer el estado antes de todo
      setState(() {
          if(widget.product.id!=3){
            print(widget.product.id);
          _selectedColorIndex = 0;
          }else{
            
          }
      });
    
    ScreenUtil.instance = ScreenUtil(
      width: 388,
      height: 1600,
      allowFontScaling: true,
    )..init(context);

    final multiplier = screenHeight / screenWidth;

    final spacer = SizedBox(height: 10.0);


void _launchMapsUrl(double lat, double lon) async {
  final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
   Image al(){
      if(widget.product.id<5){
        return Image.asset(widget.product.photos[_selectedColorIndex],height: ScreenUtil().setHeight(400) * multiplier);
      }else{
        return Image.file(File(widget.product.photos[_selectedColorIndex]),height: ScreenUtil().setHeight(400) * multiplier);
      }
    }

    final image = Hero(
      tag: widget.product.id,
      child: al()
    );

    final name = Text(
      widget.product.name.toUpperCase(),
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 20.0,
        fontWeight: FontWeight.w500,
      ),
    );

    final brand = Text(
      widget.product.brand.toUpperCase(),
      style: TextStyle(fontSize: 14.0, color: Colors.black),
    );

    final chooseColor = Text(
      "Estado",
      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500),
    );

    final colorChooser = ColorChooser(
      colors: widget.product.colors,
      selectedColorIndex: _selectedColorIndex,
      onColorSelected: (int selected) {
        setState(() {
          if(widget.product.id!=3){
            print(widget.product.id);
          _selectedColorIndex = 0;
          }else{
            print(widget.product.id);
            print(selected);
             _selectedColorIndex = selected;
             if(selected==0){
               Toast.show("Se muestra su reporte.", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
             }else{
                Toast.show("Se muestra la reparación.", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
             }
          }
        });
      },
    );


    final top = Expanded(
      child: Container(
        padding: EdgeInsets.only(top: 50.0),
        width: double.infinity,
        decoration: BoxDecoration(
          color: AppColors.scaffoldColor,
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            name,
            spacer,
            brand,
            image,
            chooseColor,
            colorChooser
          ],
        ),
      ),
    );

    final bottom = Container(
      height: MediaQuery.of(context).size.height * 0.12, // 95.0
      color: Colors.white,
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
          top: Radius.circular(20.0),
        )),
        onPressed: () {
          double lat=widget.product.lat;
          double lon=widget.product.lon;
         _launchMapsUrl(lat,lon);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              " ",
              style: TextStyle(
                fontSize: 22.0,
              ),
            ),
            Text(
              "${widget.product.price.toString()}",
              style: TextStyle(fontSize: 22.0, color: Colors.grey[600]),
            ),
          ],
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(isHome: false),
      body: Column(
        children: <Widget>[top, bottom],
      ),
    );
  }
}
