import 'dart:ui';

class AppConstants {
  static const appName = "Sinaloa Activo";
}

class AppFonts {
  static const primaryFont = "Poppins";
}

class AppFunctions {
   static Color formatColor(String color) {
    var newColor = color.substring(1).toUpperCase();
    var preffix = "0xFF";
    var finalColor = int.parse(preffix + newColor);
    return Color(finalColor);
  }
}

class AppImages {
  static const logo = "assets/images/logo.png";
  static const bacheC = "assets/images/bacheCañadas.png";
  static const watch11 = "assets/images/watch11.png";
  static const watch20 = "assets/images/bachesCampiña.png";
  static const watch21 = "assets/images/watch21.png";
  static const watch30 = "assets/images/bacheA.png";
  static const watch31 = "assets/images/bacheR.png";
  static const watch40 = "assets/images/defecto1.png";
  static const watch41 = "assets/images/watch41.png";
}
