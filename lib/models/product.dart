import '../utils/utils.dart';

class Product {
  int id;
  String name;
  List<String> photos;
  List<String> colors;
  String price;
  String brand;
  double lat,lon;

  Product(
    {
    this.id,
    this.name,
    this.price,
    this.photos,
    this.colors,
    this.brand,
    this.lat,
    this.lon,
  });
}

List<Product> products = [
  Product(
    id: 1,
    name: "R#1001",
    lat:24.782135,
    lon:-107.409927,
    price: "24.782135, -107.409927",
    photos: [AppImages.bacheC, AppImages.watch11],
    colors: ["#FE0800", "#008C3B"],
    brand: "Cañadas",
  ),
  Product(
    id: 2,
    name: "R#1002",
    lat: 24.813150,
    lon: -107.363249,
    price: "24.813150, -107.363249",
    photos: [AppImages.watch20, AppImages.watch21],
    colors: ["#FE0800", "#008C3B"],    
    brand: "La Campiña",
  ),
  Product(
    id: 3,
    name: "R#1003",
    lat: 24.808329,
    lon: -107.403670,
    price: "24.808329, -107.403670",
    photos: [AppImages.watch30, AppImages.watch31],
    colors: ["#FE0800", "#008C3B"],
    brand: "Primer Cuadro",
  ),
  Product(
    id: 4,
    name: "R#1004",
    lat: 24.842436,
    lon: -107.387316,
    price: "24.842436, -107.387316",
    photos: [AppImages.watch40, AppImages.watch41],
    colors: ["#FE0800", "#008C3B"],
    brand: "Lomas del Sol",
  ),
];
